# Test DVC Gdrive with 2 service accounts

This is to test whether it is possible to set up DVC GDrive remote with one read-only service account to be made public for pulling and one read/write service account to be made private for both pulling and pushing.

